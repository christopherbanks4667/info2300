This project search's a celebrities profession and outputs their profession
---

## Installation

npm install at the root of the project

---

## Run the project
run the script index.js in node
with two environment variables firstName and lastName.

e.g. firstName=Donald lastName=Trump node index.js 

## MIT License 

I chose the MIT license due to its wide usage and easy to understand language. I wanted it to be clear that anyone can
make use of the code in this project. 



